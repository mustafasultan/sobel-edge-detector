# Sobel Edge Detector

## Introduction

- A modular, configurable Sobel edge detector (targeting a FPGA)
- Uses a small 32x32 byte gray pixel image

## Sobel data processing

- [ ] Uses 3x3 Sobel Filters to compute X and Y components of the gradient 

## Sobel filter

- [ ] Apply 3x3 X/Y filters
    - on z0 - z8 data
    - across 32 rows, each with 32 bytes

- [ ] Generate a 32x32-bit result array, i.e,  32 x sobelVec(31:0) values

![sobelFilter](https://i.imgur.com/fMUXjfu.png)

## Sobel CSR(0) fields


| Allocated CSR(0) bits | control | status | 
| ------                | ------  | ------ |
|CSR(0)(31:16)          |   threshVal12Bit(11:0)     | |
|CSR(0)(15:8)           |    0x05    |
|CSR(0)(0)              | Assert to activate FPGA master | Deassert by FPGA for host master |


## genSobel3x3Byte Array Generation 

**genSobel3x3Byte component next state and output decode function table**

![genSobel3x3_expanded](https://i.imgur.com/tV1e8US.png)
![genSobel3x3_topLevel](https://i.imgur.com/r0XA47e.png)
## genSobel3x3Byte component NSAndOPDecode function table 

| ld0InBuf | ldNxtBufWord | selZeros | rotSobel3x3Byte |          NSArray(2)(271:0)              |          NSArray(1)(271:0)              |          NSArray(0)(271:0)              | Note              |
| ------   |    ------    |  ------  |    ------       |               ------                    |               ------                    |               ------                     |       ------      | 
|    1     |       X      |     X    |        X        |                 0                       |                 0                       |                  0                       | Synchronously load 0 in buffer (All three 272-bit words)  |
|    0     |       0      |     X    |        0        |     CSArray(2)                          |     CSArray(1)                          |      CSArray(0)                          |     No change                                                 |
|    0     |       0      |     X    |        1        | CSArray(2)(271:264) & CSArray(2)(263:0) | CSArray(1)(271:264) & CSArray(1)(263:0) | CSArray(0)(271:264) & CSArray(0)(263:0)  | Rotate all three buffer rows right 8 bits, placing lower bytes in upper byte positions |
|    0     |       1      |     0    |        X        |           BRAM32x256_dOut               |     CSArray(2)                          |     CSArray(1)                           | Load BRAM data into NSArray(2), register CSArray(1) = NSArray(2) and CSArray(0) = NSArray(1) |
|    0     |       1      |     1    |        X        |                 0                       |     CSArray(2)                          |     CSArray(1)                           | Load zeros into NSArray(2), register CSArray(1) = NSArray(2) and CSArray(0) = NSArray(1)  |

## Sobel Data Flow Diagram
![SobelDFD](https://i.imgur.com/qsvmpAO.png)

## DSPProcSobel Synthesised Top Level 
![](https://i.imgur.com/HfGuXlW.png)

## Sobel Synthesised Top Level Diagram
![SobelTopLevel](https://i.imgur.com/YzOeSNN.png)

## Sobel Finite State Machine Diagram
![SobelFSM](https://i.imgur.com/n5UYKOw.png)

