
        # AMD-Xilinx Vivado project start and tcl script: Create project, xc7z020clg400-1 technology, %lang model 

        # To execute, 

        # open cmd window 

        # cd to project folder 

        # start Vivado (with tcl file parameter) 

        # e.g, for project name DSPProcSobel 

        # cmd 

        # cd {C:/2023/HDLGenTop/myFiles/HDLGen-ChatGPT/User_Projects/DSPProc/DSPProcSobel} 

        # $vivado_bat_path -source C:/2023/HDLGenTop/myFiles/HDLGen-ChatGPT/User_Projects/DSPProc/DSPProcSobel/VHDL/AMDPrj/DSPProcSobel.tcl 


        # Vivado tcl file DSPProcSobel.tcl, created in AMDprj folder 

        cd {C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/DSPProcSobel} 

        # Close_project  Not required. Will advise that Vivado sessions should be closed. 

        start_gui

        create_project  DSPProcSobel  ./VHDL/AMDprj -part xc7z020clg400-1 -force

        set_property target_language VHDL [current_project]

        add_files -norecurse  ./VHDL/model/DSPProcSobel.vhd
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/Package/MainPackage.vhd
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/memory/memory_top/VHDL/model/memory_top.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/FSMBasedDesigns/SobelFilter/Sobel/VHDL/model/Sobel.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/memory/reg4x32_CSRA/VHDL/model/reg4x32_CSRA.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/memory/reg32x32/VHDL/model/reg32x32.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/memory/bram32X256/VHDL/model/BRAM32X256.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/memory/decodeMemWr/VHDL/model/decodeMemWr.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/FSMBasedDesigns/SobelFilter/genSobel3x3Byte/VHDL/model/genSobel3x3Byte.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/FSMBasedDesigns/SobelFilter/SobelKernel/VHDL/model/SobelKernel.vhd 
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/FSMBasedDesigns/SobelFilter/SobelFSM/VHDL/model/SobelFSM.vhd 


        update_compile_order -fileset sources_1

        set_property SOURCE_SET sources_1 [get_filesets sim_1]

        add_files -fileset sim_1 -norecurse ./VHDL/testbench/DSPProcSobel_TB.vhd

        update_compile_order -fileset sim_1

        # Remove IOBs from snthesised schematics

        current_run [get_runs synth_1]

        set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value -no_iobuf -objects [get_runs synth_1]


        # Save created wcfg in project

        set_property SOURCE_SET sources_1 [get_filesets sim_1]

        add_files -fileset sim_1 -norecurse ./VHDL/AMDPrj/DSPProcSobel_TB_behav.wcfg

        # save_wave_config {./VHDL/AMDprj/DSPProcSobel_TB_behav.wcfg}

    