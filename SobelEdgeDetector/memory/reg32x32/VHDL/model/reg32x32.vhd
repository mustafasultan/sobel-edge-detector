-- Title Section Start
-- Generated by HDLGen, Github https://github.com/fearghal1/HDLGen-ChatGPT, on 09-October-2023 at 02:13

-- Component Name : reg32x32
-- Title          : 32 x 32-bit register memory (CSRA)

-- Author(s)      : Fearghal Morgan
-- Organisation   : University of Galway
-- Email          : fearghal.morgan1@gmail.com
-- Date           : 09/10/2023

-- Description
-- 32 x 32-bit register memory
-- Addressed register is synchronously written on assertion of we (write enable).
-- Addressed register is combinationally read.

-- entity signal dictionary
-- clk	clk signal
-- rst	rst signal
-- we	synchronous write enable, asserted high
-- dIn	data input
-- dOut	addressed register output data (32-bits)
-- add	Memory address. add(4:0) (5-bits) addresses 32 x 32 registers

-- internal signal dictionary
-- NSArray	Next state 32 x 32-bit array
-- CSArray	Current 32 x 32 register array

-- Title Section End
-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;

-- entity declaration
entity reg32x32 is 
Port(
	clk : in std_logic;
	rst : in std_logic;
	we : in std_logic;
	dIn : in std_logic_vector(31 downto 0);
	dOut : out std_logic_vector(31 downto 0);
	add : in std_logic_vector(7 downto 0)
);
end entity reg32x32;

architecture RTL of reg32x32 is
-- Internal signal declarations

constant result_reg32x32_HistogramAndRangeFilter : array32x32  := 
  (31     => "00000000000000000000000000000000",      
   30     => "00000000000000000000000000000000",    
   29     => "00000000000000000000000000000000",
   28     => "00000000000000000000000000000000",    
   27     => "00000000000000000000000000000000",    
   26     => "00000000000000000000000000000000",    
   25     => "00000000000000000000000000000000",    
   24     => "00000000000000000000000000000000",    
   23     => "00000000000000000000000000000000",    
   22     => "00000000000000000000000000000000",    
   21     => "00000000000000000000000000000000",    
   20     => "00000000000000000000000000000000",    
   19     => "00000000000000000000000000000000",    
   18     => "00000000000000000000000000000000",
   17     => "00000000000000000000000000000000",    
   16     => "00000000000000000000000000000000",    
   15     => "00000000000000000000000000000000",      
   14     => "00000000000000000000000000000000",    
   13     => "00000000000000000000000000000000",
   12     => "00000000000000000000000000000000",    
   11     => "00000000000000000000000000000000",    
   10     => "00000000000000000000000000000000",    
    9     => "00000000000000000000000000000000",    
    8     => "00000000000000000000000000000000",    
    7     => "00000000000000000000000000000000",    
    6     => "00000000000000000000000000000000",    
    5     => "00000000000000000000000000000000",    
    4     => "00000000000000000000000000000000",    
    3     => "00000000000000000000000000000000",    
    2     => "00000000000000000000000000000000",    
    1     => "00000000000000000000000000000000",    
    0     => "00000000000000000000000000000000",    
   others => "00000000000000000000000000000000"
   );

signal NSArray : array32x32  := ( others => (others => '0') );
--signal CSArray : array32x32  := ( others => (others => '0') );
signal CSArray : array32x32  := result_reg32x32_HistogramAndRangeFilter;

begin

NSDecode_p: process(we,dIn,add,CSArray)
begin
	NSArray <= CSArray;-- Default assignment 
    if we = '1' then
      NSArray(to_integer( unsigned(add(4 downto 0))) ) <= dIn;
    end if;
end process;

selDOut_c: dOut <= CSArray(to_integer (unsigned(add(4 downto 0))) );

stateReg_p: process(clk,rst)
begin
	if rst = '1' then
		CSArray <= (others =>(others => '0'));
	elsif rising_edge(clk) then
		CSArray <= NSArray;
	end if;
end process;

end RTL;