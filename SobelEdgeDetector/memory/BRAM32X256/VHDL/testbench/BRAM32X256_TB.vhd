-- Title Section Start
-- VHDL testbench BRAM32X256_TB
-- Generated by HDLGen, Github https://github.com/fearghal1/HDLGen-ChatGPT, on 08-October-2023 at 21:32

-- Component Name : BRAM32X256
-- Title          : 32 x 256-bit block RAM (BRAM) memory

-- Author(s)      : Fearghal Morgan
-- Organisation   : University of Galway
-- Email          : fearghal.morgan1@gmail.com
-- Date           : 08/10/2023

-- Description    : refer to component hdl model for function description and signal dictionary
-- Title Section End
-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Testbench entity declaration. No inputs or outputs
entity BRAM32X256_TB is end entity BRAM32X256_TB;

architecture behave of BRAM32X256_TB is

-- unit under test (UUT) component declaration. Identical to component entity, with 'entity' replaced with 'component'
component BRAM32X256 is 
Port(
	rDOut : out std_logic_vector(255 downto 0);
	add : in std_logic_vector(4 downto 0);
	clk : in std_logic;
	we : in std_logic;
	dIn : in std_logic_vector(255 downto 0) 
	);
end component;

-- testbench signal declarations
signal testNo: integer; -- aids locating test in simulation waveform
signal endOfSim : boolean := false; -- assert at end of simulation to highlight simuation done. Stops clk signal generation.

-- Typically use the same signal names as in the VHDL entity, with keyword signal added, and without in/out mode keyword

signal clk: std_logic := '1';

signal rDOut : std_logic_vector(255 downto 0);
signal add : std_logic_vector(4 downto 0);
signal we : std_logic;
signal dIn : std_logic_vector(255 downto 0);

constant period: time := 20 ns; -- Default simulation time. Use as simulation delay constant, or clk period if sequential model ((50MHz clk here)
 
begin

-- Generate clk signal, when endOfSim = FALSE / 0
clkStim: clk <= not clk after period/2 when endOfSim = false else '0';

-- instantiate unit under test (UUT)
UUT: BRAM32X256-- map component internal sigs => testbench signals
port map
	(
	rDOut => rDOut, 
	add => add, 
	clk => clk, 
	we => we, 
	dIn => dIn
	);

-- Signal stimulus process
stim_p: process -- process sensitivity list is empty, so process automatically executes at start of simulation. Suspend process at the wait; statement
begin
	report "%N Simulation start, time = "& time'image(now);

	-- Apply default INPUT signal values. Do not assign output signals (generated by the UUT) in this stim_p process
	-- Each stimulus signal change occurs 0.2*period after the active low-to-high clk edge
	-- if signal type is
	-- std_logic, use '0'
	-- std_logic_vector use (others => '0')
	-- integer use 0
	add <= (others => '0');
	we <= '0';
	dIn <= (others => '0');
    wait for 0.2 * period; -- apply input signals 0.2*period after the active edge of the clk

	-- START Add testbench stimulus here
	-- === If copying a stim_p process generated by ChatGPT, delete the following lines from the beginning of the pasted code
	-- === Delete the -- === notes
	-- === stim_p: process
	-- === begin

    -- Test 1
    testNo <= 1;
    -- Note: rDout register not reset so undefined
    we <= '0';
    add <= "00000";
    dIn <= x"0000000000000000000000000000000000000000000000000000000000000000";
    wait for (1 * period);
--    assert rDOut = x"UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU"
--        report "Test 1 failed for rDout"
--        severity warning;

    -- Test 2
    testNo <= 2;
    -- Note: rDout register synchronously updated with array(0) on active clk edge.
    we <= '0';
    add <= "00000";
    dIn <= x"1f1e1d1c1b1a191817161514131211100f0e0d0c0b0a090807062b2c2de5e6ff";
    wait for (1 * period);
    assert rDOut = x"1f1e1d1c1b1a191817161514131211100f0e0d0c0b0a090807062b2c2de5e6ff"
        report "Test 2 failed for rDout"
        severity warning;

    -- Test 3
    testNo <= 3;
    -- Note: rDout register not yet updated with array(8). rDout register = array(0).
    we <= '0';
    add <= "01000";
    dIn <= x"1f1e1d1c1b1a191817161514131211100f0e0d0c0b0a090807062b2c2de5e6ff";
    wait for (0.2 * period);
    assert rDOut = x"1f1e1d1c1b1a191817161514131211100f0e0d0c0b0a090807062b2c2de5e6ff"
        report "Test 3 failed for rDout"
        severity warning;

    -- Test 4
    testNo <= 4;
    -- Note: rDout register updated with array(8).
    we <= '0';
    add <= "01000";
    dIn <= x"0000000000000000bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb0000000000000000";
    wait for (0.8 * period);
    assert rDOut = x"0000000000000000bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb0000000000000000"
        report "Test 4 failed for rDout"
        severity warning;

    -- Test 5
    testNo <= 5;
    -- Note: rDout register updated with array(1).
    we <= '1';
    add <= "00001";
    dIn <= x"deadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef";
    wait for (1 * period);
    assert rDOut = x"0000000000000000000000000000000000000000000000000000000000000000"
        report "Test 5 failed for rDout"
        severity warning;

    -- Test 6
    testNo <= 6;
    -- Note: rDout register updated with array(1).
    we <= '1';
    add <= "00001";
    dIn <= x"0000000000000000000000000000000000000000000000000000000000000000";
    wait for (1 * period);
    assert rDOut = x"deadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef"
        report "Test 6 failed for rDout"
        severity warning;


	-- ==== If copying a stim_p process generated by ChatGPT, delete the following lines from the pasted code
	-- === wait;
	-- === end process stim_p;

	-- END Add testbench stimulus here
	-- Print picosecond (ps) = 1000*ns (nanosecond) time to simulation transcript
	-- Use to find time when simulation ends (endOfSim is TRUE)
	-- Re-run the simulation for this time
	-- Select timing diagram and use View>Zoom Fit
	report "%N Simulation end, time = "& time'image(now);
	endOfSim <= TRUE; -- assert flag to stop clk signal generation

	wait; -- wait forever
end process; 
end behave;