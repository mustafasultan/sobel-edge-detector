-- Title Section Start
-- VHDL Testbench - genSobel3x3Byte_TB
-- Generated by HDLGen-ChatGPT on 27-October-2023 at 14:46
-- Github: https://github.com/HDLGen-ChatGPT/HDLGen-ChatGPT

-- Component Name:	genSobel3x3Byte
-- Title:	genSobel3x3Byte generate 3 x 3-byte data into the Sobel kernel 

-- Author(s):	Fearghal Morgan
-- Organisation:	University of Galway
-- Email:	fearghal.morgan1@gmail.com
-- Date:	27/10/2023

-- Description:	 Refer to component's HDL Model for description and signal dictionary
-- Title Section End

-- Library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;
-- Testbench entity declaration
entity genSobel3x3Byte_TB is end entity genSobel3x3Byte_TB;

architecture behave of genSobel3x3Byte_TB is

-- unit under test (UUT) component declaration. Identical to component entity, with 'entity' replaced with 'component'
component genSobel3x3Byte is 
Port(
	BRAM32x256_dOut : in std_logic_vector(255 downto 0);
	rotSobel3x3Byte : in std_logic;
	ldNxtBufWord : in std_logic;
	selZeros : in std_logic;
	ld0InBuf : in std_logic;
	clk : in std_logic;
	rst : in std_logic;
	sobel3x3Byte : out array3x24 
	);
end component;

-- testbench signal declarations
signal testNo: integer; -- aids locating test in simulation waveform
signal endOfSim : boolean := false; -- assert at end of simulation to highlight simuation done. Stops clk signal generation.

-- Typically use the same signal names as in the VHDL entity, with keyword signal added, and without in/out mode keyword

signal clk: std_logic := '1';
signal rst: std_logic;        

signal BRAM32x256_dOut : std_logic_vector(255 downto 0);
signal rotSobel3x3Byte : std_logic;
signal ldNxtBufWord : std_logic;
signal selZeros : std_logic;
signal ld0InBuf : std_logic;
signal sobel3x3Byte : array3x24;

constant period: time := 20 ns; -- Default simulation time. Use as simulation delay constant, or clk period if sequential model ((50MHz clk here)
 
begin

-- Generate clk signal, when endOfSim = FALSE / 0
clkStim: clk <= not clk after period/2 when endOfSim = false else '0';

-- Instantiate the Unit Under Test (UUT)
-- Map the component's internal signals to testbench signals
UUT: genSobel3x3Byte
port map
	(
	BRAM32x256_dOut => BRAM32x256_dOut, 
	rotSobel3x3Byte => rotSobel3x3Byte, 
	ldNxtBufWord => ldNxtBufWord, 
	selZeros => selZeros, 
	ld0InBuf => ld0InBuf, 
	clk => clk, 
	rst => rst, 
	sobel3x3Byte => sobel3x3Byte
	);

-- Signal stimulus process
-- Process automatically executes at start of simulation due to empty sensitivity list.
-- Process halts at the 'wait;' statement
stim_p: process
begin
	report "%N Simulation start, time = "& time'image(now);

	-- Apply default INPUT signal values.
	-- Each stimulus signal change occurs 0.2*period after the active low-to-high clk edge
	-- if signal type is 'std_logic', use '0'
	-- if signal type is 'std_logic_vector' use (others => '0')
	-- if signal type is 'integer' use 0
	BRAM32x256_dOut <= (others => '0');
	rotSobel3x3Byte <= '0';
	ldNxtBufWord <= '0';
	selZeros <= '0';
	ld0InBuf <= '0';
	report "Assert and toggle rst";
	testNo <= 0;
	rst    <= '1';
	wait for period*1.2; -- assert rst for 1.2*period, deasserting rst 0.2*period after active clk edge
	rst   <= '0';
	wait for period; -- wait 1 clock period
	
	-- START Testbench stimulus

	-- END Testbench stimulus

	report "%N Simulation end, time = "& time'image(now);
	-- Assert 'endOfSim' flag to stop the clock signal
	endOfSim <= TRUE;
	wait; -- Wait forever
end process; 
end behave;