-- Title Section Start
-- VHDL testbench genSobel3x3Byte_TB
-- Generated by HDLGen, Github https://github.com/fearghal1/HDLGen-ChatGPT, on 20-November-2023 at 12:50

-- Component Name : genSobel3x3Byte
-- Title          : genSobel3x3Byte generate 3 x 3-byte data into the Sobel kernel 

-- Author(s)      : Mustafa Sultan
-- Organisation   : University of Galway
-- Email          : m.sultan2@universityofgalway.ie
-- Date           : 20/11/2023

-- Description    : refer to component hdl model for function description and signal dictionary
-- Title Section End
-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;
-- Testbench entity declaration. No inputs or outputs
entity genSobel3x3Byte_TB is end entity genSobel3x3Byte_TB;

architecture behave of genSobel3x3Byte_TB is

-- unit under test (UUT) component declaration. Identical to component entity, with 'entity' replaced with 'component'
component genSobel3x3Byte is 
Port(
	BRAM32x256_dOut : in std_logic_vector(255 downto 0);
	rotSobel3x3Byte : in std_logic;
	ldNxtBufWord : in std_logic;
	selZeros : in std_logic;
	ld0InBuf : in std_logic;
	clk : in std_logic;
	rst : in std_logic;
	sobel3x3Byte : out array3x24 
	);
end component;

-- testbench signal declarations
signal testNo: integer; -- aids locating test in simulation waveform
signal endOfSim : boolean := false; -- assert at end of simulation to highlight simuation done. Stops clk signal generation.

-- Typically use the same signal names as in the VHDL entity, with keyword signal added, and without in/out mode keyword

signal clk: std_logic := '1';
signal rst: std_logic;        

signal BRAM32x256_dOut : std_logic_vector(255 downto 0);
signal rotSobel3x3Byte : std_logic;
signal ldNxtBufWord : std_logic;
signal selZeros : std_logic;
signal ld0InBuf : std_logic;
signal sobel3x3Byte : array3x24;

constant period: time := 20 ns; -- Default simulation time. Use as simulation delay constant, or clk period if sequential model ((50MHz clk here)
 
begin

-- Generate clk signal, when endOfSim = FALSE / 0
clkStim: clk <= not clk after period/2 when endOfSim = false else '0';

-- instantiate unit under test (UUT)
UUT: genSobel3x3Byte-- map component internal sigs => testbench signals
port map
	(
	BRAM32x256_dOut => BRAM32x256_dOut, 
	rotSobel3x3Byte => rotSobel3x3Byte, 
	ldNxtBufWord => ldNxtBufWord, 
	selZeros => selZeros, 
	ld0InBuf => ld0InBuf, 
	clk => clk, 
	rst => rst, 
	sobel3x3Byte => sobel3x3Byte
	);

-- Signal stimulus process
stim_p: process -- process sensitivity list is empty, so process automatically executes at start of simulation. Suspend process at the wait; statement
begin
	report "%N Simulation start, time = "& time'image(now);

	-- Apply default INPUT signal values. Do not assign output signals (generated by the UUT) in this stim_p process
	-- Each stimulus signal change occurs 0.2*period after the active low-to-high clk edge
	-- if signal type is
	-- std_logic, use '0'
	-- std_logic_vector use (others => '0')
	-- integer use 0
	BRAM32x256_dOut <= (others => '0');
	rotSobel3x3Byte <= '0';
	ldNxtBufWord <= '0';
	selZeros <= '0';
	ld0InBuf <= '0';
	report "Assert and toggle rst";
	testNo <= 0;
	rst    <= '1';
	wait for period*1.2; -- assert rst for 1.2*period, deasserting rst 0.2*period after active clk edge
	rst   <= '0';
	wait for period; -- wait 1 clock period
	
	-- START Add testbench stimulus here
	-- === If copying a stim_p process generated by ChatGPT, delete the following lines from the beginning of the pasted code
	-- === Delete the -- === notes
	-- === stim_p: process
	-- === begin


	-- ==== If copying a stim_p process generated by ChatGPT, delete the following lines from the pasted code
	-- === wait;
	-- === end process stim_p;

	-- END Add testbench stimulus here
	-- Print picosecond (ps) = 1000*ns (nanosecond) time to simulation transcript
	-- Use to find time when simulation ends (endOfSim is TRUE)
	-- Re-run the simulation for this time
	-- Select timing diagram and use View>Zoom Fit
	report "%N Simulation end, time = "& time'image(now);
	endOfSim <= TRUE; -- assert flag to stop clk signal generation

	wait; -- wait forever
end process; 
end behave;