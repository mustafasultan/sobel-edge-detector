
        # AMD-Xilinx Vivado project start and tcl script: Create project, xc7z020clg400-1 technology, %lang model 

        # To execute, 

        # open cmd window 

        # cd to project folder 

        # start Vivado (with tcl file parameter) 

        # e.g, for project name genSobel3x3Byte 

        # cmd 

        # cd {C:/2023/HDLGenTop/myFiles/HDLGen-ChatGPT/User_Projects/DSPProcSobelEE452/FSMBasedDesigns/SobelFilter/genSobel3x3Byte} 

        # $vivado_bat_path -source C:/2023/HDLGenTop/myFiles/HDLGen-ChatGPT/User_Projects/DSPProcSobelEE452/FSMBasedDesigns/SobelFilter/genSobel3x3Byte/VHDL/AMDPrj/genSobel3x3Byte.tcl 


        # Vivado tcl file genSobel3x3Byte.tcl, created in AMDprj folder 

        cd {C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/FSMBasedDesigns/SobelFilter/genSobel3x3Byte} 

        # Close_project  Not required. Will advise that Vivado sessions should be closed. 

        start_gui

        create_project  genSobel3x3Byte  ./VHDL/AMDprj -part xc7z020clg400-1 -force

        set_property target_language VHDL [current_project]

        add_files -norecurse  ./VHDL/model/genSobel3x3Byte.vhd
		add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/SobelEdgeDetector/Package/MainPackage.vhd

        update_compile_order -fileset sources_1

        set_property SOURCE_SET sources_1 [get_filesets sim_1]

        add_files -fileset sim_1 -norecurse ./VHDL/testbench/genSobel3x3Byte_TB.vhd

        update_compile_order -fileset sim_1

        # Remove IOBs from snthesised schematics

        current_run [get_runs synth_1]

        set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value -no_iobuf -objects [get_runs synth_1]


        # Save created wcfg in project

        set_property SOURCE_SET sources_1 [get_filesets sim_1]

        add_files -fileset sim_1 -norecurse ./VHDL/AMDPrj/genSobel3x3Byte_TB_behav.wcfg

        # save_wave_config {./VHDL/AMDprj/genSobel3x3Byte_TB_behav.wcfg}

    