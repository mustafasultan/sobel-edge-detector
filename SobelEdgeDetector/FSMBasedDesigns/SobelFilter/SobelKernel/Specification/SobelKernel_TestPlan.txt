Input signals                 selALUOp  A         B
Input signal radix            4'b       32'h      32'h     
Output signals                                              ALUOut    branch
Output signal radix                                         32'h      1'b
TestNo              delay                                                     Note
1                   1         0000      5a5a5a5a  15a5a5a6  70000000  0       sgnA + sgnB
2                   1         0000      15a5a5a6  5a5a5a5a  70000000  0       sgnA + sgnB
3                   1         0000      ffffffff  40000000  3fffffff  0       
4                   1         0001      ffffffff  fffffffe  00000001  0       sgnA - sgnB
5                   1         0010      f0c3a596  1f7e8ab4  10428094  0       A and B
6                   1         0011      f0c3a596  1f7e8ab4  ffffafb6  0       A or B
7                   1         0100      f0c3a596  1f7e8ab4  efbd2f22  0       A xor B
8                   1         0101      f0c3a596  00000008  c3a59600  0       A << B(4:0) shift left logical
9                   1         0110      f0c3a596  00000008  00f0c3a5  0       A >> B(4:0) shift right logical
10                  1         0111      f0c3a596  00000008  fff0c3a5  0       A >>> B(4:0) shift right arithmetic
11                  1         1000      f0c3a596  1f7e8ab4  00000001  0       1 if sgn A < sgn B
12                  1         1001      f0c3a596  1f7e8ab4  00000000  0       1 if uns A < uns B
13                  1         1010      f0c3a596  1f7e8ab4  00000000  0       branch = 1 if A = B
14                  1         1010      f0c3a596  f0c3a596  00000000  1       branch = 1 if A = B
15                  1         1011      f0c3a596  1f7e8ab4  00000000  1       branch = 1 if A != B
16                  1         1011      f0c3a596  f0c3a596  00000000  0       
17                  1         1100      f0c3a596  1f7e8ab4  00000000  1       branch = 1 if sgn A < sgnB
18                  1         1100      f0c3a596  f0c3a596  00000000  0       
19                  1         1101      f0c3a596  1f7e8ab4  00000000  0       branch = 1 if sgn A >= sgnB
20                  1         1101      f0c3a596  f0c3a596  00000000  1       
21                  1         1110      f0c3a596  1f7e8ab4  00000000  0       branch = 1 if uns A < uns B
22                  1         1110      f0c3a593  f0c3a596  00000000  1       
23                  1         1111      f0c3a596  1f7e8ab4  00000000  1       branch = 1 if uns A >= uns B
24                  1         1111      f0c3a596  f0c3a596  00000000  1      

-- === Test Specification / Test Table Notes. Update lines marked <> ===
<> Title: RV32I RISC-V processor ALU Test table (input signals and expected outputs)
<> Created by: Fearghal Morgan
<> Date: July 2023
Component type combinational or sequential (include * to highlight)
<> Combinational(*)
<> Sequential () 

Notes on the use of the Test Table with HDLGen-ChatGPT (https://github.com/fearghal1/HDLGen-ChatGPT)
1. Copy/paste the test table (excluding the header text) into the HDLGen-ChatGPT 'Test Plan' UI box.
2. Test table structure
     Use only single character spacing in this file. Do not use TAB spacing in this file.
     e.g, if using Notepad++ (https://notepad-plus-plus.org/downloads/), toggle 'make all characters visible' option to view space/TAB characters.
     Row 1 includes input signal name headings
     Row 2 includes output signal name headings
     Row 3 includes signal radix
     Row 4 includes the following column headings (Column 1: TestNo, Column 2: delay, Other: Note)
     Rows 5 onwards includes test data
3. Signal radix format: 1'b is 1-bit binary (e.g, 0, 1), 16'b is 16-bit binary (e.g, 1011111011101111), 16'h is 16-bit hexadecimal (e.g, beef).
4. Notes column text is optional, providing useful information on the individual test.
5. Sequential logic components
     clk signal: testbench HDL includes a clock (signal clk) strobe stimulus process, with default 20ns (50MHz) 'period'.
     rst (reset) signal: initially asserted, deasserting 0.2 * period after the first active clk edge.
6. Table delay values
     Combinational logic components: number of period (20ns) delays to be included, following the application of the test input signals 
     Sequential logic components:
       Number of clock periods to be applied, following  the application of the test input signals. Value can be integer or real.
       Examples:
         1:   delay 1 clk period,   ending 0.2 x period after the active clk edge
         3:   delay 3 clk period,   ending 0.2 x period after the active clk edge
         0.2: delay 0.2 clk period, ending 0.4 x period after the active clk edge
              This enables simulation ending 0.6*period prior to the next active clk edge, enabling checking of unregistered output signals
         0.8: delay 0.8 clk period, ending at the default point, i.e, 0.2 x period after the active clk edge