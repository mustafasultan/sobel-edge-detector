-- Package
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
package MainPackage is
type array4x32 is array(3 downto 0) of std_logic_vector(31 downto 0);
type array32x256 is array(31 downto 0) of std_logic_vector(255 downto 0);
type array32x32 is array(31 downto 0) of std_logic_vector(31 downto 0);
type array4x9 is array(3 downto 0) of std_logic_vector(8 downto 0);
type array3x272 is array(2 downto 0) of std_logic_vector(271 downto 0);
type array3x24 is array(2 downto 0) of std_logic_vector(23 downto 0);

component reg4x32_CSRA is
port(
	clk : in std_logic;
	rst : in std_logic;
	we : in std_logic;
	dIn : in std_logic_vector(31 downto 0);
	dArrayOut : out array4x32;
	add : in std_logic_vector(7 downto 0)
);
end component reg4x32_CSRA; 
component reg32x32 is
port(
	clk : in std_logic;
	rst : in std_logic;
	we : in std_logic;
	dIn : in std_logic_vector(31 downto 0);
	dOut : out std_logic_vector(31 downto 0);
	add : in std_logic_vector(7 downto 0)
);
end component reg32x32; 
component accumBytesWord0 is
port(
	clk : in std_logic;
	rst : in std_logic;
	BRAM_dOut : in std_logic_vector(255 downto 0);
	CSRA0 : in std_logic_vector(31 downto 0);
	wr : out std_logic;
	add : out std_logic_vector(7 downto 0);
	datToMem : out std_logic_vector(31 downto 0)
);
end component accumBytesWord0; 
component BRAM32X256 is
port(
	rDOut : out std_logic_vector(255 downto 0);
	add : in std_logic_vector(7 downto 0);
	we : in std_logic;
	dIn : in std_logic_vector(255 downto 0);
	clk : in std_logic
);
end component BRAM32X256; 
component decodeMemWr is
port(
	wr : in std_logic;
	reg4x32_CSRA_we : out std_logic;
	add : in std_logic_vector(7 downto 0);
	reg32x32_we : out std_logic;
	BRAM32x256_we : out std_logic
);
end component decodeMemWr; 
component memory_top is
port(
	clk : in std_logic;
	rst : in std_logic;
	wr : in std_logic;
	add : in std_logic_vector(7 downto 0);
	datToMem : in std_logic_vector(31 downto 0);
	reg32x32_dOut : out std_logic_vector(31 downto 0);
	BRAM32x256_rDOut : out std_logic_vector(255 downto 0);
	CSRA : out array4x32
);
end component memory_top; 
component histogramAndRangeFilter is
port(
	clk : in std_logic;
	rst : in std_logic;
	BRAM_dOut : in std_logic_vector(255 downto 0);
	CSRA : in array4x32;
	wr : out std_logic;
	add : out std_logic_vector(7 downto 0);
	datToMem : out std_logic_vector(31 downto 0);
	reg32x32_dOut : in std_logic_vector(31 downto 0)
);
end component histogramAndRangeFilter; 
component SobelKernel is
port(
	threshVal12Bit : in std_logic_vector(11 downto 0);
	sobelBit : out std_logic;
	sobel3x3Byte : in array3x24
);
end component SobelKernel; 
component genSobel3x3Byte is
port(
    BRAM32x256_dOut : in std_logic_vector(255 downto 0);
    rotSobel3x3Byte : in std_logic;
    ldNxtBufWord : in std_logic;
    selZeros : in std_logic;
    ld0InBuf : in std_logic;
    clk : in std_logic;
    rst : in std_logic;
    sobel3x3Byte : out array3x24
);
end component genSobel3x3Byte; 
component SobelFSM is
port(
    clk : in std_logic;
    rst : in std_logic;
    reg4x32_CSRA : in array4x32;
    wr : out std_logic;
    add : out std_logic_vector(7 downto 0);
    datToMem : out std_logic_vector(31 downto 0);
    rotSobel3x3Byte : out std_logic;
    ldNxtBufWord : out std_logic;
    selZeros : out std_logic;
    ld0InBuf : out std_logic;
    sobelBit : in std_logic
);
end component SobelFSM; 
component Sobel is
port(
	clk : in std_logic;
	rst : in std_logic;
	BRAM32x256_dOut : in std_logic_vector(255 downto 0);
	reg4x32_CSRA : in array4x32;
	wr : out std_logic;
	add : out std_logic_vector(7 downto 0);
	datToMem : out std_logic_vector(31 downto 0)
);
end component Sobel; 

end MainPackage;
package body MainPackage is
end MainPackage;

